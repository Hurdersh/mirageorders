<!DOCTYPE html>
<html lang="it" dir="ltr">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>

    <link type="text/css" rel="stylesheet" href="css/master.css"/>
    <link type="icon" rel="icon" href="img/palm.png"/>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title>Mirage Products</title>
  </head>
  <body onload="refreshChat(500)" onresize="resize()">
    <!--Navbar-->
    <?php
    session_start();
    if(isset($_SESSION['auth'])){
      if($_SESSION['admin']){
        echo '
            <nav class="orange">
              <div class="nav-wrapper">
		<a href="#!" class="brand-logo"><i class="material-icons">beach_access</i>Mirage</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                  <li><a href="ordini.php">Ordini</a></li>
                  <li><a href="comunicazionia.php">Comunicazioni</a></li>
                  <li><a href="clienti.php">Clienti</a></li>
                  <li><a href="php/logout.php">Esci</a></li>
                </ul>
              </div>
            </nav>

            <ul class="sidenav" id="mobile-demo">
              <li><a href="ordini.php">Ordini</a></li>
              <li><a href="comunicazionia.php">Comunicazioni</a></li>
              <li><a href="clienti.php">Clienti</a></li>
              <li><a href="php/logout.php">Esci</a></li>
            </ul>

        ';
     }
    }
    else{
      header('Location: login.php');
    }
    ?>
    <!--Navbar-->

    <!--
    <form action="php/chatinsert.php" method="post">
      <input type="submit">
    </form>
  -->
    <?php
        session_start();
        if(isset($_SESSION['auth'])){

          if($_SESSION['admin']){
            echo '


            <div class="row">
              <div class="chatcontainer col s12" id="chatcontainer">


              </div>
            </div>
                <div class="row m0">
                  <div class="input-field col s9 m11 m0">
                    <i class="material-icons prefix">mode_edit</i>
                    <textarea id="messaggio" class="materialize-textarea"></textarea>
                    <label for="messaggio">Messaggio</label>
                  </div>
                  <div class="input-field col s3 m1 m0">
                    <button onmousedown="insertMessage()" class="btn waves-effect waves-light" type="submit" name="action">Invia
                      <i class="material-icons right">send</i>
                    </button>
                  </div>
                </div>
            ';

          }

        }
        else{
          header("Location: login.php");
        }
    ?>

    <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
    <script type="text/javascript" src="js/refreshcoma.js"></script>
    <script type="text/javascript" src="js/comainsert.js"></script>
    <script type="text/javascript" src="js/auto-resize.js"></script>
  </body>
</html>
