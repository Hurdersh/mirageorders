<?php
  session_start();
  if(isset($_SESSION['auth'])){
    include "generic_query.php";
    if($_SESSION['admin']){
      $conversations = genericQuerySelect("127.0.0.1","root","16caratterialfanumerici","mirage_products",["conversazioni.idc","conversazioni.tempo","conversazioni.effettuata","messaggi.n_ombrellone","messaggi.tipo","users.nome","users.cognome"],["users","messaggi","conversazioni"],"users.n_ombrellone=messaggi.n_ombrellone AND users.tipo = messaggi.tipo AND messaggi.idc = conversazioni.idc AND conversazioni.n_ombrellone = -1 AND conversazioni.tipo = 0 GROUP BY n_ombrellone,tipo ORDER BY effettuata,tempo;");
      $text = "";
      if($conversations != -1){

        for ($x=0; $x < sizeof($conversations); $x++) {
          $tempo_ex = explode(" ",$conversations[$x]['tempo']);
          $nombrellone = $conversations[$x]['n_ombrellone'];
          $nome = $conversations[$x]['nome'];
          $cognome = $conversations[$x]['cognome'];
          $tempo = $conversations[$x]['tempo'];
          $splittedtime = explode(" ",$tempo);
          $splitteddate = explode("-",$splittedtime[0]);
          $splittedhour = explode(":",$splittedtime[1]);
          $date = $splitteddate[2] ."/".$splitteddate[1] ."/".$splitteddate[0];
          $hour = $splittedhour[0].":".$splittedhour[1];

          $tipo = "Ombrellone";
          if($conversations[$x]['tipo'] > 0){
            $tipo = "Palma";
          }
          $chatbutton = '<form target="_blank" name="formchat" method="post" action="chatgeneric.php"><button name="submit" id="chatbutton" class="btn waves-effect waves-light blue" type="submit" value="'.$nombrellone.':'.$conversations[$x]['tipo'].'" name="action">Chat
  </button></form>';
          $fattobutton = '<a id="fattobutton" onmousedown="fattoOrder('.$conversations[$x]['idc'].')" class="waves-effect waves-light btn green">Fatto</a>';
          $colore = "#ffb74d orange lighten-2";
          $deletebutton = '<a onmousedown="activate_modals('.$nombrellone.',\''.$tipo. '\',' . $conversations[$x]['idc'].",".$conversations[$x]['tipo'].')" class="waves-effect waves-light btn red">Elimina</a>';
          if($conversations[$x]['effettuata']){
            $colore = "green";
            $fattobutton = '<a id="fattobutton" onmousedown="deFattoOrder('.$conversations[$x]['idc'].')" class="waves-effect waves-light btn #ffb74d orange lighten-2"> Fare</a>';
          }

          $minutes = explode(":",$tempo_ex[1]);
          $text .=
          '
          <div class="row">
            <div class="col s12 m12 ">
              <div class="card '. $colore .'">
                <div class="card-content m0">

                    <div class="row m0">
                      <div class="col mlchat m0">
                        <h6>' . "N° " . $nombrellone . " " . $tipo . "</br></br>" . $nome . " " . $cognome .'</h6>
                      </div>
                    </div>


                    <p class="right-align m0"><small>'. $date."   ".$hour .'</small></p>


                </div>

                <div class="card-action m0">
                  <div class="row">

                    <div class="col">
                        '.$deletebutton.'
                    </div>
                    <div class="col">
                        '.$fattobutton.'
                    </div>

                    <div class="col ">
                          '.$chatbutton.'
                    </div>




                  </div>

                </div>

              </div>
            </div>
          </div>';
        }
        echo $text;
      }
      //// TODO: message of start orders
    }
    else{
      die("death");
    }
  }
  else{
    die("you should log in");
  }
?>
