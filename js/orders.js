nuovo = false;
temptext = 0;


function refreshOrders(ms){
  setInterval(printChat, ms);
}

function printChat(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/ordersrefresh.php", true);
  xmlhttp.onreadystatechange = function() {
          console.log('refreshing');
          if (this.readyState == 4 && this.status == 200) {
              var container = document.getElementById("ordinicontainer");
              if(this.responseText.length != temptext){
                nuovo = true;
                temptext = this.responseText.length;
              }
              if(nuovo){
                container.innerHTML = this.responseText;
                nuovo = false;
              }
          }
  };
  xmlhttp.send();

}

function fattoOrder(idc){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/orderfatto.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            console.log("conversation "+idc+" flagged fatto");
          }
  };
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var idcpost = "idc="+idc;
  xmlhttp.send(idcpost);

}

function deFattoOrder(idc){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/orderdefatto.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            console.log("conversation "+idc+" flagged fatto");
          }
  };
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var idcpost = "idc="+idc;
  xmlhttp.send(idcpost);

}


function deleteOrder(nombrellone,tipo,idc){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/orderdelete.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            console.log("conversation "+idc+" deleted");
          }
  };
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var idcpost = "idc="+idc+"&"+"nombrellone="+nombrellone+"&"+"tipo="+tipo;
  xmlhttp.send(idcpost);

}
