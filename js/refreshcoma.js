nuovo = false;
temptext = 0;

newnotify = false;
messages = 0;
check = 0;

function refreshChat(ms){
  scrollDown();
  setInterval(printChat, ms);
}

function printChat(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/comunication.php", true);
  xmlhttp.onreadystatechange = function() {
          console.log('refreshing');
          if (this.readyState == 4 && this.status == 200) {
              var container = document.getElementById("chatcontainer");
              console.log(this.responseText.length);
              console.log(temptext);
              if(this.responseText.length != temptext){
                nuovo = true;
                temptext = this.responseText.length;
              }
              if(nuovo){
                container.innerHTML = this.responseText;
                nuovo = false;
              }
          }
  };
  xmlhttp.send();

}

function removecoma(idm){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/removecoma.php", true);
  xmlhttp.onreadystatechange = function() {
          console.log('deleted');
  };
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var idm = "idm="+idm;
  xmlhttp.send(idm);
}

function scrollDown(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/comunication.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              var container = document.getElementById("chatcontainer");
              container.innerHTML = this.responseText;
              temptext = this.responseText.length;
          }
  };
  xmlhttp.send();
}
