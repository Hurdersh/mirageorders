<!DOCTYPE html>
<html lang="it" dir="ltr">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>
    <link type="icon" rel="icon" href="img/palm.png"/>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title>Mirage Products</title>
  </head>
  <body>
    <!--Navbar-->
    <?php
    session_start();
    if(isset($_SESSION['auth'])){
     if($_SESSION['admin']){
        header("Location: ordini.php");
     }
     else{
       echo '
            <nav class="orange">
              <div class="nav-wrapper">
      		<a href="#!" class="brand-logo"><i class="material-icons">beach_access</i>Mirage</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                  <li><a href="index.php"><i class="material-icons left">home</i>Home</a></li>
                  <li><a href="chat.php"><i class="material-icons left">chat</i>Chat</a></li>
                  <li><a href="comunicazioni.php"><i class="material-icons left">bookmark</i>Comunicazioni</a></li>
                  <li><a href="php/logout.php">Esci</a></li>
                </ul>
              </div>
            </nav>

            <ul class="sidenav" id="mobile-demo">
            <li><a href="index.php"><i class="material-icons left">home</i>Home</a></li>
            <li><a href="chat.php"><i class="material-icons left">chat</i>Chat</a></li>
            <li><a href="comunicazioni.php"><i class="material-icons left">bookmark</i>Comunicazioni</a></li>
            <li><a href="php/logout.php">Esci</a></li>
            </ul>
          '
        ;
     }
    }
    else{
      header('Location: login.php');
    }
    if($_SESSION['auth']){
	    echo '
			<p class="left-align">*per ordinare basta aprire la chat e scrivere i piatti di cui ci si vuole cibare</p>
			<h1 class="center-align">Menu\' fisso</h1>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Frittura Mista</h4> <h4 style="float: right;">9€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Frittura Calamari</h4> <h4 style="float: right;">12€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Spiedini di Pesce</h4> <h4 style="float: right;">9€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Calamari Gratinati</h4> <h4 style="float: right;">6€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Arrosticini</h4> <h4 style="float: right;">0.60€ caduno</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Cotoletta e Patatine</h4> <h4 style="float: right;">5€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Patatine Fritte</h4> <h4 style="float: right;">2€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Insalata di Mare</h4> <h4 style="float: right;">5€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Verdure Grigliate</h4> <h4 style="float: right;">2.50€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Insalata di Riso</h4> <h4 style="float: right;">5€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Insalatone</h4> <h4 style="float: right;">4.50€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Caprese</h4> <h4 style="float: right;">4€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Prosciutto e Melone</h4> <h4 style="float: right;">5€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Scaloppine Funghi/Limone</h4> <h4 style="float: right;">5€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Insalata "Mirage" </h4> <h4 style="float: right;">6€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Cozze Fritte </h4> <h4 style="float: right;">5€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Formaggio Fritto</h4> <h4 style="float: right;">5€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Straccetti di pollo</h4> <h4 style="float: right;">5€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Hamburger e Patatine</h4> <h4 style="float: right;">5€</h4>
			</div>
	    		<div style="overflow: hidden;">
				<h4 style="float: left;">-  Pasta al Pomodoro</h4> <h4 style="float: right;">4€</h4>
			</div>
			
			<p>Questa App facilita l\'esistenza ai clienti,alle bariste ed alle cuoche</br>si puo\' 
			anche ordinare senza alzarsi dal lettino/sdraio</p></br>
			<p> Un grazie speciale a Venanzio per esistere e divertire tutti attraverso le sue mirabolanti avventure</p>	
			<div>
				<p class="center-align"><img class="responsive-img" src="img/venanzio.jpg"></p>
			</div>
        		
		';
	    	

    }
    ?>
    <!--Navbar-->
    <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
  </body>
</html>
