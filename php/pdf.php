<?php
  use setasign\Fpdi\Fpdi;
  use setasign\Fpdi\PdfReader;
  include "fpdf/fpdf.php";
  include "fpdi/src/autoload.php";
  session_start();
  if(isset($_SESSION['auth'])){
    if($_SESSION['admin']){
      $password = $_POST['password'];
      //TODO relative
      $path = "password_" . $password;
      $filename = "/home/debian/pdf/password_" . $password;
      $pdf = new FPDI();
      $pageCount = $pdf->setSourceFile('mirage_istruzioni.pdf');
      $pageId = $pdf->importPage(1, PdfReader\PageBoundaries::MEDIA_BOX);

      $pdf->addPage();
      $pdf->useImportedPage($pageId, 0, 0, 200);
      $pdf->SetFont('Arial','B',16);
      $pdf->SetTextColor(255,0,0);
      $pdf->Text(60,45,$password);
      $pdf->Output('F',$filename);
      echo $path;
    }
  }
  else{
    header("location: ../login.php");
  }
?>
