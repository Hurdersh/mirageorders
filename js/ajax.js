nuovo = false;
temptext = 0;

newnotify = false;
messages = 0;
check = 0;

function refreshChat(ms){
  scrollDown();
  setInterval(printChat, ms);
  setInterval(checkNotify,ms);
}

function printChat(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/chatrefresh.php", true);
  xmlhttp.onreadystatechange = function() {
          console.log('refreshing');
          if (this.readyState == 4 && this.status == 200) {
              var container = document.getElementById("chatcontainer");
              if(this.responseText.length > temptext){
                nuovo = true;
                temptext = this.responseText.length;
              }
              if(nuovo){
                container.innerHTML = this.responseText;
                setTimeout(function(){console.log(container);container.scrollTop = container.scrollHeight;},500);
                nuovo = false;
              }
          }
  };
  xmlhttp.send();

}

function scrollDown(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/chatrefresh.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              var container = document.getElementById("chatcontainer");
              container.innerHTML = this.responseText;
              container.scrollTop = container.scrollHeight;
              temptext = this.responseText.length;
          }
  };
  xmlhttp.send();
}

function checkNotify(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/usernotification.php", true);
  xmlhttp.onreadystatechange = function() {
          console.log('checking notify');

          if (this.readyState == 4 && this.status == 200) {
              if(this.responseText.length > messages){
                newnotify = true;
                messages = this.responseText.length;
              }
              if(newnotify && check > 0){
                notify();
                newnotify = false;
              }
              else if(check == 0){
                newnotify = false;
              }
              check=check+1;
          }
  };
  xmlhttp.send();
}

function notify() {
        Push.create('Mirage Products', {
        body: 'Nuovo Messaggio',
        icon: 'img/palm.png',
        link: 'chat.php',
        onClick: function () {
            console.log("Fired!");
            window.focus();
            this.close();
        }
	});
}
