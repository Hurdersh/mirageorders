<!DOCTYPE html>
<html lang="it" dir="ltr">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>

    <link type="text/css" rel="stylesheet" href="css/master.css"/>
    <link type="icon" rel="icon" href="img/palm.png"/>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title>Mirage Products</title>
  </head>
  <body class="m0" onload="refreshComunication(500)" onresize="resize()">
    <!--Navbar-->
    <?php
    session_start();
    if(isset($_SESSION['auth'])){
      if($_SESSION['admin']){
        echo '
            <nav class="orange">
              <div class="nav-wrapper">
		<a href="#!" class="brand-logo"><i class="material-icons">beach_access</i>Mirage</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                  <li><a href="ordini.php">Ordini</a></li>
                  <li><a href="comunicazionia.php">Comunicazioni</a></li>
                  <li><a href="clienti.php">Clienti</a></li>
                  <li><a href="php/logout.php">Esci</a></li>
                </ul>
              </div>
            </nav>

            <ul class="sidenav" id="mobile-demo">
              <li><a href="ordini.php">Ordini</a></li>
              <li><a href="comunicazionia.php">Comunicazioni</a></li>
              <li><a href="clienti.php">Clienti</a></li>
              <li><a href="php/logout.php">Esci</a></li>
            </ul>

            <!-- Modal -->
            <div id="delete" class="modal">
              <div class="modal-content">
                <h4 id="modalcheck"></h4>
                <p>Se sei sicuro Clicca Elimina,Verranno Eliminati anche i messaggi relativi all\' utente</p>
              </div>
              <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Annulla</a>
                <a href="#!" id="securedelete" class="modal-close waves-effect waves-green btn-flat red-text">Elimina</a>
              </div>
            </div>

        ';
     }
    }
    else{
      header('Location: login.php');
    }
    ?>
    <!--Navbar-->

    <!--
    <form action="php/chatinsert.php" method="post">
      <input type="submit">
    </form>
  -->
    <?php
        session_start();
        if(isset($_SESSION['auth'])){
          $_SESSION['messages'] = '';

          if($_SESSION['admin']){
            echo '
            <div class="row m0" style="background-color: #ecedbe !important;">
              <div class="col">
                <a onmousedown="location.href=\'addcliente.php\'" class="waves-effect waves-light btn"><i class="material-icons left">add</i>Nuovo Cliente</a>
              </div>
            </div>

            <div class="row m0">
              <div class="comunicationcontainer col s12" id="clienticontainer">

              </div>
            </div>
            ';

          }
        }
        else{
          header("Location: login.php");
        }
    ?>

    <script type="text/javascript" src="js/activate_modal.js"></script>
    <script type="text/javascript" src="js/collapsible.js"></script>
    <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
    <script type="text/javascript" src="js/clienti.js"></script>
    <script type="text/javascript" src="js/auto-resize.js"></script>
  </body>
</html>
