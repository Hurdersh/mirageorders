nuovo = false;
temptext = 0;
function refreshComunication(ms){
  firstComunication();
  setInterval(printComunication, ms);
}

function firstComunication(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/comunication.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              console.log('first comunication');
              var container = document.getElementById("comunicationcontainer");
              container.innerHTML = this.responseText;
              temptext = this.responseText.length;
          }
  };
  xmlhttp.send();

}

function printComunication(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/comunication.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              console.log('refreshing comunications');
              var container = document.getElementById("comunicationcontainer");
              console.log(this.responseText.length);
              console.log(temptext);
              if(this.responseText.length > temptext){
                notify();
                nuovo = true;
                temptext = this.responseText.length;
              }
              else if(this.responseText.length < temptext){
                nuovo = true;
                temptext = this.responseText.length;
              }
              if(nuovo){
                container.innerHTML = this.responseText;
                nuovo = false;
              }
          }
  };
  xmlhttp.send();

}

function notify() {
        Push.create('Mirage Products', {
        body: 'Nuova Comunicazione',
        icon: 'img/palm.png',
        link: '/mirage_products/comunicazioni.php',
        onClick: function () {
            console.log("Fired!");
            window.focus();
            this.close();
        },
    });
}
