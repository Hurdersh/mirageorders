<!DOCTYPE html>
<html lang="it" dir="ltr">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>

    <link type="text/css" rel="stylesheet" href="css/master.css"/>
    <link type="icon" rel="icon" href="img/palm.png"/>


    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title>Mirage Products</title>
  </head>
  <body class="m0" onresize="resize()">
    <!--Navbar-->
    <?php
    session_start();
    if(isset($_SESSION['auth'])){
      if($_SESSION['admin']){
        echo '
            <nav class="orange">
              <div class="nav-wrapper">
		<a href="#!" class="brand-logo"><i class="material-icons">beach_access</i>Mirage</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                  <li><a href="ordini.php">Ordini</a></li>
                  <li><a href="comunicazionia.php">Comunicazioni</a></li>
                  <li><a href="clienti.php">Clienti</a></li>
                  <li><a href="php/logout.php">Esci</a></li>
                </ul>
              </div>
            </nav>

            <ul class="sidenav" id="mobile-demo">
              <li><a href="ordini.php">Ordini</a></li>
              <li><a href="comunicazionia.php">Comunicazioni</a></li>
              <li><a href="clienti.php">Clienti</a></li>
              <li><a href="php/logout.php">Esci</a></li>
            </ul>
        ';
     }
     else{
       echo '
            <nav class="orange">
              <div class="nav-wrapper">
		<a href="#!" class="brand-logo"><i class="material-icons">beach_access</i>Mirage</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                  <li><a href="chat.php">Chat</a></li>
                  <li><a href="comunicazioni.php">Home</a></li>
                  <li><a href="php/logout.php">Esci</a></li>
                </ul>
              </div>
            </nav>

            <ul class="sidenav" id="mobile-demo">
              <li><a href="chat.php">Chat</a></li>
              <li><a href="comunicazioni.php">Home</a></li>
              <li><a href="php/logout.php">Esci</a></li>
            </ul>
          '
        ;
     }
    }
    else{
      header('Location: login.php');
    }
    ?>
    <!--Navbar-->

    <!--
    <form action="php/chatinsert.php" method="post">
      <input type="submit">
    </form>
  -->
    <?php
        session_start();
        if(isset($_SESSION['auth'])){
          $_SESSION['messages'] = '';

          if($_SESSION['admin']){
            echo '
            <form method="post" action="php/addclienteb.php" onsubmit="return checkClient();">
                  <div class="row">
                    <div class="input-field col s4 m4">
                      <input  id="nome" type="text" name="nome" class="validate" required>
                      <label for="nome">Nome</label>
                    </div>
                    <div class="input-field col s4 m4">
                      <input id="cognome" type="text" name="cognome" class="validate" required>
                      <label for="cognome">Cognome</label>
                    </div>
                    <div class="input-field col s4 m3">
                      <input id="ntelefono" type="text" name="ntelefono" class="validate">
                      <label for="ntelefono">Telefono</label>
                    </div>
                  </div>
                  <div class="row">
                    <div class="input-field col s4">
                      <input id="nombrellone" type="number" name="nombrellone" value="1" min="1" max="356" class="validate">
                      <label for="nombrellone">Numero Ombrellone</label>
                    </div>

                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="input-field ">
                        <p>
                         <label>
                           <input id="tipo" name="tipo" type="checkbox" class="m0"/>
                           <span>Palma</span>
                         </label>
                       </p>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col offset-s5 ">

                      <button class="btn waves-effect waves-light" type="submit" value="pipa" name="action">Submit
                        <i class="material-icons right">send</i>
                      </button>

                    </div>
                  </div>
            </form>


            ';

          }
        }
        else{
          header("Location: login.php");
        }
    ?>
    <script type="text/javascript" src="js/clienti.js"></script>
    <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
    <script type="text/javascript" src="js/auto-resize.js"></script>
  </body>
</html>
