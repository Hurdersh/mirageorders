<!DOCTYPE html>
<html lang="it" dir="ltr">
  <head>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="materialize/css/materialize.min.css"  media="screen,projection"/>

    <link type="text/css" rel="stylesheet" href="css/master.css"/>
    <link type="icon" rel="icon" href="img/palm.png"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="utf-8">
    <title>Mirage Products</title>
  </head>
  <body>
    <!--Navbar-->
    <?php
      session_start();
      if(isset($_SESSION['auth'])){
        if($_SESSION['admin']){
          echo '
              <nav class="orange">
                <div class="nav-wrapper">
		  <a href="#!" class="brand-logo"><i class="material-icons">beach_access</i>Mirage</a>
                  <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                  <ul class="right hide-on-med-and-down">
                    <li><a href="ordini.php">Ordini</a></li>
                    <li><a href="comunicazioni.php">Comunicazioni</a></li>
                    <li><a href="clienti.php">Clienti</a></li>
                    <li><a href="php/logout.php">Esci</a></li>
                  </ul>
                </div>
              </nav>

              <ul class="sidenav" id="mobile-demo">
                <li><a href="ordini.php">Ordini</a></li>
                <li><a href="comunicazioni.php">Comunicazioni</a></li>
                <li><a href="clienti.php">Clienti</a></li>
                <li><a href="logout.php">Esci</a></li>
              </ul>

          ';
       }
       else{
         echo '
              <nav class="orange">
                <div class="nav-wrapper">
		  <a href="#!" class="brand-logo"><i class="material-icons">beach_access</i>Mirage</a>
                  <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                  <ul class="right hide-on-med-and-down">
                    <li><a href="chat.php"><i class="material-icons">chat</i>Chat</a></li>
                    <li><a href="comunicazioni.php">Home</a></li>
                  </ul>
                </div>
              </nav>

              <ul class="sidenav" id="mobile-demo">
                  <li><a href="chat.php">Chat</a></li>
                  <li><a href="comunicazioni.php">Home</a></li>
              </ul>
            '
          ;
       }
      }
      else{
        echo '
            <nav class="orange">
              <div class="nav-wrapper">
		<a href="#!" class="brand-logo"><i class="material-icons">beach_access</i>Mirage</a>
                <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
                <ul class="right hide-on-med-and-down">
                  <li><a href="login.php">Accedi</a></li>
                </ul>
              </div>
            </nav>

	   <ul class="sidenav" id="mobile-demo">
		   <li><a href="chat.php"><i class="material-icons left">chat</i>Chat</a></li>
		   <li><a href="comunicazioni.php"><i class="material-icons left">bookmark</i>Comunicazioni</a></li>
		   <li><a href="php/logout.php">Esci</a></li>
           </ul>

            '
          ;
     }
    ?>
    <!--Navbar-->
    <?php
      session_start();
      if(isset($_SESSION['auth'])){
        $_SESSION['error'] = "non ti puoi loggare,lo sei gia'";
        $_SESSION['bool'] = true;
        if(isset($_SESSION['error']) and $_SESSION['bool']){
          echo '<h3 class="center-align" style="color: red;">'. $_SESSION['error'] .'</h3>';
          $_SESSION['bool'] = false;
        }
      }
      else{
          if(isset($_SESSION['error']) and $_SESSION['bool']){
            echo '<h3 class="center-align" style="color: red;">'. $_SESSION['error'] .'</h3>';
            $_SESSION['bool'] = false;
          }
          echo '
          <form method="post" name="form" action="php/loginb.php">
            <div class="row">
              <div class="col s12 m6 offset-m3">
                <div class="card white darken-1">
                  <div class="card-content">
                    <span class="card-title">Accedi</span>
                    <div style="margin:0;" class="row">
                      <div class="input-field col s12">
                        <input id="password" name="password" type="password" class="validate" minlength="6" maxlength="6" required>
                        <label for="password">Password</label>
                      </div>
                    </div>

                  </div>
                  <div class="card-action">
                    <div class="center-align">
                      <button class="btn waves-effect waves-light" type="submit" value="submit" name="submit">Accedi
                        <i class="material-icons right">send</i>
                      </button>
                    </div>

                  </div>
                </div>
              </div>
            </div>
          </form>

              ';
      }
    ?>


    <script type="text/javascript" src="materialize/js/materialize.min.js"></script>
    <script type="text/javascript" src="js/navbar.js"></script>
  </body>
</html>
