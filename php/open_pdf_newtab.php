<?php
  session_start();
  if(isset($_SESSION['auth'])){
    if($_SESSION['admin']){
      $filename = $_GET['filename'];
      header("Content-type: application/pdf");
      header("Content-Disposition: inline; filename=". $filename);
      @readfile('/home/debian/pdf/'. $filename);
    }
  }
  else{
    header("location: ../login.php");
  }
?>
