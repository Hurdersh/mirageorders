function activate_modals(nombrellone,tipo,idc,tipoint){
      var modal = document.getElementById('delete');
      var modalcheck = document.getElementById('modalcheck');
      modalcheck.innerHTML = "Eliminare L\'ordine per " + tipo + " " +nombrellone;
      var securedelete = document.getElementById('securedelete');
      securedelete.setAttribute("onmousedown","deleteOrder("+nombrellone+","+tipoint+"," + idc +")");
      var instance = M.Modal.init(modal);
      instance.open();
}

function activate_modals_client(nombrellone,tipo,tipoint){
  var modal = document.getElementById('delete');
  var modalcheck = document.getElementById('modalcheck');
  modalcheck.innerHTML = "Eliminare Il cliente per " + nombrellone + " " +tipo;
  var securedelete = document.getElementById('securedelete');
  securedelete.setAttribute("onmousedown","deleteClient("+nombrellone+","+tipoint+")");
  var instance = M.Modal.init(modal);
  instance.open();
}
