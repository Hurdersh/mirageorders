nuovo = false;
temptext = 0;
function refreshComunication(ms){
  firstComunication();
  setInterval(printComunication, ms);
}

function firstComunication(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/clients.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              console.log('first comunication');
              var container = document.getElementById("clienticontainer");
              container.innerHTML = this.responseText;
              temptext = this.responseText.length;
          }
  };
  xmlhttp.send();

}

function printComunication(){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/clients.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              console.log('refreshing comunications');
              var container = document.getElementById("clienticontainer");
              console.log(this.responseText.length);
              console.log(temptext);
              if(this.responseText.length > temptext){
                nuovo = true;
                temptext = this.responseText.length;
              }
              else if(this.responseText.length < temptext){
                nuovo = true;
                temptext = this.responseText.length;
              }
              if(nuovo){
                container.innerHTML = this.responseText;
                nuovo = false;
              }
          }
  };
  xmlhttp.send();

}

function deleteClient(nombrellone,tipo){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/clientdelete.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            console.log("client "+nombrellone+" deleted");
          }
  };
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var idcpost = "nombrellone="+nombrellone+"&"+"tipo="+tipo;
  xmlhttp.send(idcpost);

}



function checkClient(){
  var xmlhttp = new XMLHttpRequest();
  var nombr = document.getElementById('nombrellone');
  var ti = document.getElementById('tipo');
  var ntelefono = document.getElementById('ntelefono');
  var nombrellone = nombr.value;
  var tipo = ti.value;
  if(ti.checked){
    tipo = 1;
  }
  else{
    tipo = 0;
  }
  xmlhttp.open("POST", "php/checkclientdb.php", false);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            if(this.responseText == "false"){
              if(!nombr.classList.contains("invalid")){
                nombr.classList.add("invalid");
              }
            }
            else{
              if(nombr.classList.contains("invalid")){
                nombr.classList.remove("invalid");
              }

            }
          }
  };
  if(ntelefono.value != ""){
    if(!checkphone(ntelefono.value)){
      ntelefono.classList.add("invalid");
      return false;
    }
  }
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var idcpost = "nombrellone="+nombrellone+"&"+"tipo="+tipo;
  xmlhttp.send(idcpost);
  if(nombr.classList.contains("invalid")){
    return false;
  }
  else{
    console.log("true");
    return true;
  }
}

function checkphone(number){
  return number.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im);
}

function create_pdf(password){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.open("POST", "php/pdf.php", true);
  xmlhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            console.log("pdf with "+password+" created");
	    console.log("porco " + this.responseText + "di qua");
	    window.open("php/open_pdf_newtab.php?filename="+this.responseText);
          }
  };
  xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
  var pdfdata = "password="+password;
  xmlhttp.send(pdfdata);

}
