<?php
  //conditions is a string
  //return -1 if fails else return a matrix
  function genericQuerySelect($server,$username,$password,$dbname,$fields,$tables,$conditions){
    //connect to DB
    $conn = new mysqli($server, $username, $password, $dbname);
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    //query Construction
    $query = "SELECT ";
    foreach ($fields as $value) {

      $query .= "$value,";
    }
    $query = substr($query, 0, -1);
    $query .= " FROM ";
    foreach ($tables as $value) {
      $query .= "$value,";
    }
    $query = substr($query, 0, -1);
    if($conditions != ""){
      $query .= " WHERE ";
      $query .= $conditions;
      $query .= ";";
    }
    //this is the helpfull string in all the fucking world
    //echo $query;
    //do the query and return data
    $result = $conn->query($query);
    $data = [];
    if ($result->num_rows > 0) {
      while($row = $result->fetch_assoc()) {
        $data[] = $row;
      }
      //close connection of DB
      $conn->close();
      return $data;
    }
    else {

      //close connection of DB
      $conn->close();
      return -1;
    }

  }



  function genericQueryAdd($server,$username,$password,$dbname,$table,$fields,$values){
    //connect to DB
    $conn = new mysqli($server, $username, $password, $dbname);
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    //query Construction
    $query = "INSERT INTO $table(";
    foreach ($fields as $value) {
      $query .= "$value,";
    }
    $query = substr($query, 0, -1);
    $query .= ") VALUES( ";
    foreach ($values as $value) {
      //substitute ' with '' for parameters
      $value = str_replace("'","''",$value);
      if($value == "true" or $value == "false" or $value == "NOW()"){
        $query .= "$value,";
      }
      else{
        $query .= "'$value',";
      }
    }
    $query = substr($query, 0, -1);
    $query .= ");";
    //echo $query;
    //do the query and return data
    if ($conn->query($query) === TRUE) {
      //close connection of DB
      $conn->close();
      return 0;
    }
    else {
      $conn->close();
      return -1;
    }
  }

  function genericQueryUpdate($server,$username,$password,$dbname,$fields,$values,$tables,$conditions){
    //connect to DB
    $conn = new mysqli($server, $username, $password, $dbname);
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    //query Construction
    $query = "UPDATE ";
    foreach ($tables as $value) {
      $query .= "$value,";
    }
    $query = substr($query, 0, -1);
    $query.= " SET ";
    if(sizeof($fields) == sizeof($values)){
      for($x=0 ;$x<sizeof($fields);$x++){
        $field = $fields[$x];
        $value = $values[$x];
        if($value != 'NOW()'){
          $query .= "$field='$value',";
        }
        else{
          $query .= $field.'='.$value.',';
        }
      }
    }
    else{
      return -2;
    }
    $query = substr($query, 0, -1);
    if($conditions != ""){
      $query .= " WHERE ";
      $query .= $conditions;
      $query .= ";";
    }
    //echo $query;
    if ($conn->query($query) === TRUE) {
      //close connection of DB
      $conn->close();
      return 0;
    }
    else {
      $conn->close();
      return -1;
    }
  }

  function genericQueryDelete($server,$username,$password,$dbname,$tables,$conditions){
    //connect to DB
    $conn = new mysqli($server, $username, $password, $dbname);
    if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
    }

    //query Construction
    $query = "DELETE FROM ";
    foreach ($tables as $value) {
      $query .= "$value,";
    }
    $query = substr($query, 0, -1);
    if($conditions != ""){
      $query .= " WHERE ";
      $query .= $conditions;
      $query .= ";";
    }
    //echo $query;
    if ($conn->query($query) === TRUE) {
      //close connection of DB
      $conn->close();
      return 0;
    }
    else {
      $conn->close();
      return -1;
    }
  }

?>
